#============================================================================
# SmartCoaster.py
#
# Python library for interfacing with the sensors of a V1.0.0 SmartCoaster
#  running MicroPython v1.13 (2020-09-11) running on an ESP8266.
#
# Created by M.Graves (2020-10-19)
#============================================================================
import json

from machine import Pin, I2C
import time

#============================================================================
#
# Constant Definitions
#
#============================================================================
I2C_SCL_PIN =			Pin(14)
I2C_SDA_PIN =			Pin(2)
TEMP_I2C_ADDR = 		0x48
TEMP_INT_PIN =			Pin(4, Pin.IN, Pin.PULL_UP)
MASS_I2C_ADDR =			0x2A
MASS_INT_PIN =			Pin(5, Pin.IN, Pin.PULL_UP)

#============================================================================
#
# Register Definitions
#
#============================================================================
TEMP_REG_TEMP =			0x00				# 00h - Temperature Register			https://www.ti.com/lit/ds/symlink/tmp116.pdf#page=25&zoom=100,0,42
TEMP_REG_CONFIG =		0x01				# 01h - Configuration Register

MASS_REG_PU_CTRL =		0x00				# 00h - Power Up Control
MASS_REG_CTRL1 =		0x01				# 01h - Control Register 1
MASS_REG_CTRL2 =		0x02				# 02h - Control Register 2
MASS_REG_ADCO_B2 =		0x12				# 12h - ADC Conversion Result Byte2
MASS_REG_ADC =			0x15				# 15h - ADC Registers

class SmartCoaster:
	_initialized = False
	_i2c = None
	_config = {
		"config_file":			"config.json",	# Set to "" to skip loading a configuration file.
		"run_self_test":		True,

		"i2c_frequency":		400000,

		"temp_cal_scale":		0.0078125,		# degC/bit
		"temp_cal_offset":		-6.7359,		# degC (For ambient)

		"mass_cal_scale":		0.0894682641,	# g/unit
		"mass_cal_offset":		-1431467.05514, # g

		"mass_gain":			0b111,			# 128x
		"mass_rate":			0b000,			# 10sps
	}

	def __init__(self, i2c=None, **config):
		#********************************************************************
		# Initialize Configuration Dictionary
		#********************************************************************
		try:
			if("config_file" in config and config["config_file"] != ""):
				# Configuration file provided to constructor, attempt to load 
				#  this instead of the default ("config.json")
				with open(config["config_file"], "r") as config_file:
					self._config.update(json.load(config_file))
			elif("config_file" in self._config and self._config["config_file"] != ""):
				# No configuration file provided, attempt to load the default.
				with open(self._config["config_file"], "r") as config_file:
					self._config.update(json.load(config_file))
		except (OSError, ValueError):
			# OSError 		- File doesn't exist, or could not open it
			# ValueError 	- File not json, or JSON could not be decoded
			pass

		self._config.update(config)

		#********************************************************************
		# Initialize I2C connection
		#********************************************************************
		if(i2c == None or type(i2c) is not I2C):
			# I2C object not provided, or i2c is not of type <I2C>
			self._i2c = I2C(scl=I2C_SCL_PIN, sda=I2C_SDA_PIN, freq=self._config["i2c_frequency"])

		#********************************************************************
		# Run Self Test
		#********************************************************************
		if(self._config["run_self_test"]):
			if(not self._SelfTest()):
				print("Warning: Self Test Failed!")
				Exception("SelfTest Failed!")
		
		#********************************************************************
		# Initialize Sensors
		#********************************************************************
		if(not self._InitSensors()):
			self._initialized = False
			print("Warning: Sensor Initialization Failed!")
			Exception("Failed To Initialize Sensors!")
		self._initialized = True

	#========================================================================
	# Board Functions
	#========================================================================

	def _SelfTest(self):
		#********************************************************************
		# Basic self test to ensure device hardware is functioning/present.
		#
		# Returns True if test is successful, or False if anything failed.
		#********************************************************************
		print("Performing SelfTest..")
		result = True
		scan = self._i2c.scan()
		
		if(TEMP_I2C_ADDR not in scan):
			print("\tWarning: I2C scan did not find Temperature Sensor (TMP116) at address: 0x{0:02x} ({0})".format(TEMP_I2C_ADDR))
			result = False
		else:
			print("\tFound Temperature Sensor (TMP116) at address: 0x{0:02x} ({0})".format(TEMP_I2C_ADDR))
			
		if(MASS_I2C_ADDR not in scan):
			print("\tWarning: I2C scan did not find Load Cell Sensor (NAU7802) at address: 0x{0:02x} ({0})".format(MASS_I2C_ADDR))
			result = False
		else:
			print("\tFound Load Cell Sensor (NAU7802) at address: 0x{0:02x} ({0})".format(MASS_I2C_ADDR))
		
		print("SelfTest Complete!")
		return result

	def _InitSensors(self):
		#********************************************************************
		# Basic function to configure the sensors with their initial states.
		#
		# Returns True if routine is successful, otherwise False.
		#********************************************************************
		print("Initializing Sensors..")
		print("\tInitializing Temperature Sensor (TMP116)..")
		print("\tInitializing Mass Sensor (NAU7802)..")
		# Reset all register values
		self._RegWrite(MASS_I2C_ADDR, MASS_REG_PU_CTRL, b'\x01')		# Set reset register bit
		time.sleep(0.001)
		self._RegWrite(MASS_I2C_ADDR, MASS_REG_PU_CTRL, b'\x00')		# Clear reset register bit
		
		# Power up the digital/analog power supplies
		self._RegWrite(MASS_I2C_ADDR, MASS_REG_PU_CTRL, b'\x06')
		
		# Wait until power up is complete
		if(not self._RegReadUntil(MASS_I2C_ADDR, MASS_REG_PU_CTRL, 1, 0x08, 0x08)):	
			print("\t\tWarning: Timed out while waiting for Load Cell Sensor (NAU7802) to power up")
			self._initialized = False
			return False
			
		# Turn on  the internal AVDD LDO
		self._RegWrite(MASS_I2C_ADDR, MASS_REG_PU_CTRL, b'\x80', b'\x80') # Set the LDO bit
		
		# Set the gain (default: 128x)
		bGain = bytes([self._config["mass_gain"]]) # 128x = 0bxxxxx111
		self._RegWrite(MASS_I2C_ADDR, MASS_REG_CTRL1, bGain, bGain)

		# Set the sample rate (default: 10sps)
		bRate = bytes([self._config["mass_rate"] << 4]) # 10sps = 0bx000xxxx
		self._RegWrite(MASS_I2C_ADDR, MASS_REG_CTRL2, bRate, bRate)	
		
		# Turn off CLK_CHP. From 9.1 power on sequencing.
		self._RegWrite(MASS_I2C_ADDR, MASS_REG_CTRL2, b'\x02', b'\x02') # Set the CALS bits to 1

		print("Initialization Complete!")
		return True


	#========================================================================
	#
	# I2C Helper Functions
	#
	#========================================================================
	def _RegRead(self, i2cAddr: int, regAddr: int, regSize: int=1):
		return self._i2c.readfrom_mem(i2cAddr, regAddr, regSize)

	def _RegReadInt(self, i2cAddr:int, regAddr: int, regSize: int=1) -> int:
		#********************************************************************
		# Read the register at <regAddr:int> of size <regSize:int>, return
		#  the value as a positive <int>.
		#********************************************************************
		return int.from_bytes(self._RegRead(i2cAddr, regAddr, regSize), regSize)

	def _RegWrite(self, i2cAddr:int, regAddr: int, val: bytes, mask: bytes=b'') -> None:
		#********************************************************************
		# Write <val:bytes> & <mask:int> to the register at <regAddr:int>.
		#  if <mask:bytes> == b'', this will overwrite the register (mask = 0xFF)
		#********************************************************************
		if(mask != b''):
			assert(len(mask) == len(val),"Length of <mask:bytes> and <val:bytes> must be the same")
			regBytes = self._i2c.readfrom_mem(i2cAddr, regAddr, len(val))
			self._i2c.writeto_mem(i2cAddr, regAddr, bytes([(regBytes[i] & ~mask[i]) | (val[i] & mask[i]) for i in range(len(val))]))
		else:
			self._i2c.writeto_mem(i2cAddr, regAddr, val)

	def _RegReadUntil(self, i2cAddr:int, regAddr: int, regSize: int, mask: int, val: int, timeout: int=10):
		#********************************************************************
		# Read the register at <regAddr:int> of size <regSize:int> every ms, 
		#  until the (reg & mask) == val, or <timeout> (ms) elapses. Return
		#  True if the equality is true, false if a timeout occurs.
		#********************************************************************
		while(timeout > 0):
			# Read the PU_CTRL register, check if Power Up Ready bit is set.
			if(self._RegRead(i2cAddr, regAddr, regSize)[0] & mask == val):
				return True
			time.sleep(0.001)
			timeout -= 1
		return False

	#========================================================================
	#
	# Temperature Sensor Functions - TMP116
	#
	#========================================================================
	def _RegReadTemp(self) -> int:
		#********************************************************************
		# Read the temperature register and return the value as an unscaled
		#  positive <int>.
		#********************************************************************
		return self._RegReadInt(TEMP_I2C_ADDR, TEMP_REG_TEMP, 2)

	def _PinReadTempAlert(self) -> int:
		#********************************************************************
		# Read the ALERT pin and return the value as a <bool> (True = active)
		#********************************************************************
		return not TEMP_INT_PIN.value()

	def GetTemperature(self) -> float:
		#********************************************************************
		# Gets the current temperature from the TMP116 and returns the scaled
		#  temperature as a <float> in degrees Celsius.
		#********************************************************************
		return (self._RegReadTemp() * self._config["temp_cal_scale"]) + self._config["temp_cal_offset"]

	#========================================================================
	#
	# Load Cell (Mass) Sensor Functions - NAU7802
	#
	#========================================================================
	def _RegSetCycleStart(self):
		#************************************************************************
		# Function to start a measurement cycle. Result is ready when CycleReady
		#  is True.
		#************************************************************************
		self._RegWrite(MASS_I2C_ADDR, MASS_REG_PU_CTRL, b'\x08', b'\x08') # Set the Cycle Start bit
		
	def _RegReadCycleReady(self):
		#************************************************************************
		# Function to read if the current measurement cycle is ready to be read.
		#************************************************************************
		
		return self._RegReadInt(MASS_I2C_ADDR, MASS_REG_PU_CTRL) & 0x10 == 0x10

	def _RegWaitCycleReady(self):
		return self._RegReadUntil(MASS_I2C_ADDR, MASS_REG_PU_CTRL, 1, ~(0x10), 0x10, timeout=1000)

	def _RegReadCycleResult(self):
		#************************************************************************
		# Function to read if the current measurement result. Assumes CycleReady is
		#  true prior to reading.
		#************************************************************************
		
		# Write address of register to device (Beginning of Burst Read)
		self._i2c.writeto(MASS_I2C_ADDR, bytes([MASS_REG_ADCO_B2]))
		
		# Read 3Bytes back from device
		return int.from_bytes(self._i2c.readfrom(MASS_I2C_ADDR, 3),3)

	def GetMass(self) -> float:
		#********************************************************************
		# Gets the current mass from the NAU7802 and returns the scaled
		#  mass as a <float> in degrees grams.
		#********************************************************************
		return (self._RegReadCycleResult() * self._config["mass_cal_scale"]) + self._config["mass_cal_offset"]