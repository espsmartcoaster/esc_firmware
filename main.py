import network
from SmartCoaster   import *

class Application:
    # Default Configuration
    _config = {
        # Any values defined in this dictionary here will be merged with this json file.
        "config_file":      "config.json",
    }
    _sc = None

    def __init__(self):
        #============================================================================
        # Load configuration from file 
        #============================================================================
        try:
            print("Attempting to laod configuration from file: '{config_file}'..".format(**self._config))
            with open(self._config["config_file"], "r") as config_file:
                self._config.update(json.load(config_file))
                print("Configuration successfully loaded!")
        except (OSError, ValueError, TypeError):
            # OSError 		- File doesn't exist, or could not open
            # ValueError 	- File not json, or JSON could not be decoded
            # TypeError     - load() didn't return a dictionary
            print("Failed! Falling back to default configuration!")
            pass

        if(self._sc == None):
            self._sc = SmartCoaster(confg=_config)

    def Start(self):
        while (True):
            r = [self.sc.GetTemperature()]
            for i in range(10):
                start = time.ticks_ms()
                r = r + [self.sc.GetMass()]
                while(time.ticks_diff(time.ticks_ms(), start) < 100):
                    time.sleep(0.001)
            r = r + [sum(r[1:])/10]
            print(r)
	

app = Application()

app.Start()